using PrimitivesLimits;

namespace Test_Programming_intro
{
    [TestClass]
    public class NumberTests
    {
        //isOdd
        [TestMethod]
        public void IsOddTestPositiveOdd()
        {
            //Arrange
            const bool expected = true;
            const int testNumber = 5;
            
            //Test
            bool result = NumberMethods.IsOdd(testNumber);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsOddTestPositiveEven()
        {
            //Arrange
            const bool expected = false;
            const int testNumber = 12;

            //Test
            bool result = NumberMethods.IsOdd(testNumber);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsOddTestNegativeOdd()
        {
            //Arrange
            const bool expected = true;
            const int testNumber = -9;

            //Test
            bool result = NumberMethods.IsOdd(testNumber);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsOddTestNegativeEven()
        {
            //Arrange
            const bool expected = false;
            const int testNumber = -4;

            //Test
            bool result = NumberMethods.IsOdd(testNumber);

            //Assert
            Assert.AreEqual(expected, result);
        }

        //isPrime
        [TestMethod]
        public void IsPrimeTestPrimeNum()
        {
            //Arrange
            const bool expected = true;
            const int testNumber = 7;

            //Test
            bool result = NumberMethods.IsPrime(testNumber);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsPrimeTestNonPrimeNum()
        {
            //Arrange
            const bool expected = false;
            const int testNumber = 28;

            //Test
            bool result = NumberMethods.IsPrime(testNumber);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsPrimeTestZero()
        {
            //Arrange
            const bool expected = false;
            const int testNumber = 0;

            //Test
            bool result = NumberMethods.IsPrime(testNumber);

            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void IsPrimeTestNegative()
        {
            //Arrange
            const bool expected = false;
            const int testNumber = -3;

            //Test
            bool result = NumberMethods.IsPrime(testNumber);

            //Assert
            Assert.AreEqual(expected, result);
        }

        //minElement
        [TestMethod]
        public void MinElementTestFullArray()
        {
            const int expected = 0;
            int[] array = { 2, 43, 12, 6, 8, 1, 0, 21 };

            int result = NumberMethods.MinElement(array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MinElementTestEmptyArray()
        {
            const int expected = -1;
            int[] array = {};

            int result = NumberMethods.MinElement(array);

            Assert.AreEqual(expected, result);
        }

        //KthMin element
        [TestMethod]
        public void KthMinElementTestFullArray()
        {
            const int expected = 8;
            int[] array = { 2, 43, 12, 6, 8, 1, 0, 21 };
            int k = 4;

            int result = NumberMethods.KthMinElement(k, array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void KthMinElementTestEmptyArray()
        {
            const int expected = -1;
            int[] array = {};
            int k = 4;

            int result = NumberMethods.KthMinElement(k, array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void KthMinElementOutOfBounds()
        {
            const int expected = -1;
            int[] array = { 2, 43, 12, 6, 8, 1, 0, 21 };
            int k = -1;

            int result = NumberMethods.KthMinElement(k, array);

            Assert.AreEqual(expected, result);
        }

        //GetOddOccurences
        [TestMethod]
        public void GetOddOccurencesTest()
        {
            const int expected = 5;
            int[] array = { 3, 1, 5, 2, 1, 2, 3 };

            int result = NumberMethods.GetOddOccurences(array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetOddOccurencesTestNoOddOcc()
        {
            const int expected = -1;
            int[] array = { 3, 1, 5, 5, 2, 1, 2, 3 };

            int result = NumberMethods.GetOddOccurences(array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetOddOccurencesTestEmpty()
        {
            const int expected = -1;
            int[] array = {  };

            int result = NumberMethods.GetOddOccurences(array);

            Assert.AreEqual(expected, result);
        }

        //getAverage
        [TestMethod]
        public void GetAverageTestIntResult()
        {
            const int expected = 3;
            int[] array = { 1, 2, 3, 4, 5 };

            int result = NumberMethods.GetAverage(array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetAverageTestFloatResult()
        {
            const int expected = 3;
            int[] array = { 1, 2, 3, 4, 5,6 };

            int result = NumberMethods.GetAverage(array);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetAverageTestEmptyArr()
        {
            const int expected = 0;
            int[] array = { };

            int result = NumberMethods.GetAverage(array);

            Assert.AreEqual(expected, result);
        }

        //pow
        [TestMethod]
        public void PowTestPositive()
        {
            long expected = 8192;
            int number = 2;
            int power = 13;

            long result = NumberMethods.Pow(number, power);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PowTestNegative()
        {
            long expected = -27;
            int number = -3;
            int power = 3;

            long result = NumberMethods.Pow(number, power);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PowTestPowZero()
        {
            long expected = 1;
            int number = 12;
            int power = 0;

            long result = NumberMethods.Pow(number, power);

            Assert.AreEqual(expected, result);
        }

        //factoriel
        [TestMethod]
        public void DoubleFactorielTest()
        {
            long expected = 720;
            int num = 3;

            long result = NumberMethods.DoubleFactoriel(num);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void KthFactorielTest()
        {
            long expected = 720;
            int num = 3;
            int k = 2;
            long result = NumberMethods.KthFactoriel(k, num);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LowestDivNumTestPositive()
        {
            long expected = 60;
            int num = 5;

            long result = NumberMethods.LowestDivisibleNumber(num);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LowestDivNumTestNegative()
        {
            long expected = 1;
            int num = -4;

            long result = NumberMethods.LowestDivisibleNumber(num);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LowestDivNumTestZero()
        {
            long expected = 0;
            int num = 0;

            long result = NumberMethods.LowestDivisibleNumber(num);

            Assert.AreEqual(expected, result);
        }
    }
}